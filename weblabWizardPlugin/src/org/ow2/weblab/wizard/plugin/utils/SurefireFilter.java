package org.ow2.weblab.wizard.plugin.utils;

import java.io.File;
import java.io.FileFilter;

public class SurefireFilter implements FileFilter {

	@Override
	public boolean accept(File pathname) {
		return (pathname.getName().endsWith(".xml"));
	}
	
}
