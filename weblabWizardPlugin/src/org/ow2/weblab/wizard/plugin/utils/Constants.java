/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2012 Cassidian, an EADS company
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.wizard.plugin.utils;

/**
 * Constants use in the WebLab Plugin
 * @author Clément Caron - Cassidian
 */
public final class Constants {
	private Constants() {}
	
	//Common
	public final static String MVN = "mvn";
	public final static String TINY_WEBLAB_LOGO = "/resources/images/weblab.png";
	public final static String WEBLAB_LOGO = "/resources/images/logo-weblab.png";
	public final static String CATALOG_XML = "/resources/catalogs/catalog.xml";
	public final static String INDENT_XSLT = "/resources/xsl/indent.xsl";
	public final static String DEFAULT_SETTINGS = "/resources/settings/WebLabSettings.xml";
	public final static String PREFIX_SETTINGS = "WebLabSettings";
	public final static String SUFFIX_SETTINGS = ".xml";
	public final static String PLUGIN_ID = "weblabWizardPlugin";
	
	//Preferences
	public final static String MVN_PATH = "WEBLAB_MVN_PATH";
	public final static String MVN_SETTINGS = "WEBLAB_MVN_SETTINGS";
	public final static String MVN_MAVEN_OPTS = "WEBLAB_MVN_MAVEN_OPTS";
	public final static String MVN_CREATE_OPTS = "WEBLAB_MVN_CREATE_OPTS";
	public final static String MVN_ECLIPSE_OPTS = "WEBLAB_MVN_ECLIPSE_OPTS";
	public final static String MVN_PACKAGE_OPTS = "WEBLAB_MVN_PACKAGE_OPTS";
	public final static String MVN_INSTALL_OPTS = "WEBLAB_MVN_INSTALL_OPTS";
	public final static String CATALOG_SETTINGS = "WebLabCatalogSettings";
	
	//WeblabWizard
	public final static String MVN_CREATE_ARCHETYPE = "archetype:create";
	public final static String MVN_ARCHETYPE_ARTIFACT_ID = "-DarchetypeArtifactId=";
	public final static String MVN_CREATE_WEBLAB_ARCHETYPE = "org.ow2.weblab.tools.maven:weblab-archetype-plugin:generate";
	public final static String MVN_SERVICE_NAME = "-DserviceName=";
	public final static String MVN_WEBLAB_INTERFACE = "-DwebLabInterface=";
	public final static String MVN_WEBLAB_FIX_COMMAND = "-DinteractiveMode=false";//not use anymore
	public final static String MVN_GROUP_ID = "-DgroupId=";
	public final static String MVN_ARTIFACT_ID = "-DartifactId=";
	
	public final static String MVN_ERROR = "Something went wrong during mvn invocation";
	
	//PageOne
	public final static String PAGE_ONE_NAME = "Page One";
	public final static String PAGE_ONE_TITLE = "Page One";
	public final static String PAGE_ONE_DESCRIPTION = "First Page of the WebLab Wizard";
	public final static String MVN_NOT_FOUND = "Maven program was not found.";
	public final static String MVN_NOT_FOUND2 = "Please download the program on <a href=\"http://maven.apache.org/\">Maven WebSite</a>";
	public final static String MVN_NOT_FOUND3 = "and set the Maven path in Preferences or here:";
	public final static String MVN_FOUND = "Maven program was successfully found";
	public final static String MVN_TEST = " -version";
	public final static String BROWSE_BUTTON = "Browse for Maven";
	
	//PageTwo
	public final static String DESCRIPTION_STARTER= "Description :";
	public final static String PAGE_TWO_NAME = "Page Two";
	public final static String PAGE_TWO_TITLE = "Page Two";
	public final static String PAGE_TWO_DESCRIPTION = "Second Page of the WebLab Wizard";
	public final static String WEBLAB_COMBO_NAME = "WebLab Service Interface";
	public final static String WEBLAB_COMBO_DESC = "WebLab Web Service Creation Tool.\nChoose your interface(s) and fill the fields,\nthen finish the creation of your project";
	public final static String REGEX_WARN_PACKAGE = "([a-z]([a-zA-Z0-9])*)(\\.[a-z]([a-zA-Z0-9])*)*";
	public final static String REGEX_ERROR_PACKAGE = "([a-zA-Z]([a-zA-Z0-9])*)(\\.[a-zA-Z]([a-zA-Z0-9])*)*";
	public final static String REGEX_WARN_CLASSNAME = "([A-Z]([a-zA-Z0-9])*)";
	public final static String REGEX_ERROR_CLASSNAME = "([a-zA-Z]([a-zA-Z0-9])*)";//^(([a-z])+.)+[A-Z]([A-Za-z])+$
	public final static String REGEX_ERROR_ARTIFACT = "^[^\\\\\\/\\?\\*\\\"\\>\\<\\:\\|]*$";//Windows Folder ^[^\\\/\?\*\"\>\<\:\|]*$
	// Linux Folder
	
	//PreferencePage1
	public final static String PREFERENCE_PAGE1_DESCRIPTION = "WebLab Settings";
	public final static String MVN_PATH_DESC = "MVN &Path:";
	public final static String MVN_SETTINGS_DESC = "&Settings file:";
	public final static String MVN_MAVEN_OPTS_DESC = "&Maven_opts:";
	public final static String MVN_CREATE_OPTS_DESC = "&Create_opts:";
	public final static String MVN_ECLIPSE_OPTS_DESC = "&Eclipse_opts:";
	public final static String MVN_PACKAGE_OPTS_DESC = "P&ackage_opts:";
	public final static String MVN_INSTALL_OPTS_DESC = "&Install_opts:";
	
	//PreferencePage2
	public final static String PREFERENCE_PAGE2_DESCRIPTION = "WebLab Catalog";
	public final static String CATALOG_SETTINGS_DESC = "Entities";
	
	//CatalogEditor
	public final static String SERIALISATION_LINE_SEPARATOR = "ø";
	public final static String SERIALISATION_FIELD_SEPARATOR = "§";
	public final static String ADD_BUTTON_TEXT = "&Add";
	public final static String EDIT_BUTTON_TEXT = "&Edit";
	public final static String REMOVE_BUTTON_TEXT = "&Remove";
	public final static String IMPORT_BUTTON_TEXT = "&Import";
	public final static String EXPORT_BUTTON_TEXT = "E&xport";
	public final static int STARING_TIME = 3000;
	public final static String POPUP_DIALOG_TITLE = "Catalog Entity";
	public final static String POPUP_DIALOG_INFO = "©WebLab";
	public final static int CURSOR_SIZE = 15; //PopupDialog
	public final static int ARTIFACT_ID_COLUMN_WIDTH = 160;
	public final static int GROUP_ID_COLUMN_WIDTH = 130;
	public final static int VERSION_COLUMN_WIDTH = 40;
	
	//catalog.xml
	public final static String CATALOG = "catalog";
	public final static String ARCHETYPE_CATALOG = "archetype-catalog";
	public final static String ARCHETYPES = "archetypes";
	public final static String ARCHETYPE = "archetype";
	public final static String ARTIFACT_ID = "artifactId";
	public final static String GROUP_ID = "groupId";
	public final static String VERSION = "version";
	public final static String REPOSITORY = "repository";
	public final static String DESCRIPTION = "description";
}
