/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2012 Cassidian, an EADS company
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.wizard.plugin.utils;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

/**
 * Popup to Add or Edit a Catalog Entity
 * @author Clément Caron - Cassidian
 */
public class PopupCatalogEntity {
	private Text groupId;
	private Text artifactId;
	private Text version;
	private Text repository;
	private Text description;
	private final Shell parent;
	private final CatalogEditor editor;
	private TableItem tableItem = null;
	private Shell popupContainer;
	private Button okButton;
	
	/**
	 * Constructor
	 * @param editor The Catalog Table
	 * @param shell The Shell containing the Editor & the Popup
	 */
	public PopupCatalogEntity(CatalogEditor editor, Shell shell) {
		this.parent = shell;
		this.editor = editor;
		createPopup();
	}
	
	/**
	 * Create the Popup
	 */
	private void createPopup() {
		popupContainer = new Shell(parent);
		popupContainer.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				popupContainer.getParent().setEnabled(true);
			}
		});
		popupContainer.setSize(350, 265);
		popupContainer.setLocation(parent.getLocation().x
				+ parent.getSize().x / 2 - popupContainer.getSize().x / 2,
				parent.getLocation().y
						+ parent.getSize().y / 2
						- popupContainer.getSize().y / 2);

		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		popupContainer.setLayout(layout);
		
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		Label labelArtifactId = new Label(popupContainer, SWT.NULL);
		labelArtifactId.setText("ArtifactId");
		artifactId = new Text(popupContainer, SWT.BORDER | SWT.SINGLE);
		artifactId.setText("");
		artifactId.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				testPopupComplete();
			}
		});
		artifactId.setLayoutData(gd);

		gd = new GridData(GridData.FILL_HORIZONTAL);
		Label labelGroupId = new Label(popupContainer, SWT.NULL);
		labelGroupId.setText("GroupId");
		groupId = new Text(popupContainer, SWT.BORDER | SWT.SINGLE);
		groupId.setText("");
		groupId.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				testPopupComplete();
			}
		});
		groupId.setLayoutData(gd);
		
		gd = new GridData(GridData.FILL_HORIZONTAL);
		Label labelVersion = new Label(popupContainer, SWT.NULL);
		labelVersion.setText("Version");
		version = new Text(popupContainer, SWT.BORDER | SWT.SINGLE);
		version.setText("");
		version.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				testPopupComplete();
			}
		});
		version.setLayoutData(gd);
		
		gd = new GridData(GridData.FILL_HORIZONTAL);
		Label labelRepository = new Label(popupContainer, SWT.NULL);
		labelRepository.setText("Repository");
		repository = new Text(popupContainer, SWT.BORDER | SWT.SINGLE);
		repository.setText("");
		repository.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				testPopupComplete();
			}
		});
		repository.setLayoutData(gd);
		
		gd = new GridData(GridData.FILL_BOTH);
		Label labelDescription = new Label(popupContainer, SWT.NULL);
		labelDescription.setText("Description");
		description = new Text(popupContainer, SWT.BORDER | SWT.MULTI);
		description.setText("");
		description.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				testPopupComplete();
			}
		});
		description.setLayoutData(gd);
		
		Composite buttonContainer = new Composite(popupContainer, SWT.NULL);
//		buttonContainer.setSize(popupContainer.getSize().x, buttonContainer.getSize().y);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 2;
		buttonContainer.setLayoutData(gd);
		layout = new GridLayout(2, true);
		buttonContainer.setLayout(layout);
		
		okButton = new Button(buttonContainer, SWT.PUSH);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		okButton.setLayoutData(gd);
		okButton.setText("Ok");
		okButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				widgetDefaultSelected(e);
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				save();
				close();
			}
		});
		
		Button cancelButton = new Button(buttonContainer, SWT.PUSH);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		cancelButton.setLayoutData(gd);
		cancelButton.setText("Cancel");
		cancelButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				widgetDefaultSelected(e);
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				close();
			}
		});
		
		popupContainer.setSize(350, 265);
	}

	/**
	 * Open an Add Popup
	 */
	public void openAddPopup() {
		tableItem = null;
		popupContainer.getParent().setEnabled(false);
		popupContainer.setText("Add Catalog Entry");
		okButton.setText("Add");
		popupContainer.open();
	}
	
	/**
	 * Open an Edit Popup for an Entity
	 * @param item Catalog Entity
	 */
	public void openEditPopup(TableItem item) {
		if(item.getData() instanceof CatalogEntity) {
			tableItem = item;
			CatalogEntity entity = (CatalogEntity) item.getData();
			groupId.setText(entity.getGroupId());
			artifactId.setText(entity.getArtifactId());
			version.setText(entity.getVersion());
			repository.setText(entity.getRepository());
			description.setText(entity.getDescription());
			popupContainer.getParent().setEnabled(false);
			popupContainer.setText("Edit Catalog Entry");
			okButton.setText("Edit");
			popupContainer.open();
		}
	}
	
	/**
	 * Close the popup
	 */
	public void close() {
		popupContainer.getParent().setEnabled(true);
		popupContainer.dispose();
	}
	
	/**
	 * Save Or Create an Entity
	 */
	protected void save() {
		if(tableItem == null) {
			editor.createEntity(getArtifactId(), getGroupId(), getVersion(), getRepository(), getDescription());
		}
		else {
			if(tableItem.getData() instanceof CatalogEntity) {
				CatalogEntity entity = (CatalogEntity) tableItem.getData();
				entity.setArtifactId(getArtifactId());
				entity.setGroupId(getGroupId());
				entity.setVersion(getVersion());
				entity.setRepository(getRepository());
				entity.setDescription(getDescription());
				
				tableItem.setText(new String[] {getArtifactId(), getGroupId(), getVersion()});
			}
		}
	}

	/**
	 * Test if enough information were given
	 */
	protected void testPopupComplete() {
		// TODO Auto-generated method stub
	}
	
	
	public String getGroupId() {
		if(groupId != null)
			return groupId.getText();
		return null;
	}
	public void setGroupId(String groupId) {
		this.groupId.setText(groupId);
	}
	
	public String getArtifactId() {
		if(artifactId != null)
			return artifactId.getText();
		return null;
	}
	public void setArtifactId(String artifactId) {
		this.artifactId.setText(artifactId);
	}

	public String getVersion() {
		if(version != null)
			return version.getText();
		return null;
	}
	public void setVersion(String version) {
		this.version.setText(version);
	}

	public String getRepository() {
		if(repository != null)
			return repository.getText();
		return null;
	}
	public void setRepository(String repository) {
		this.repository.setText(repository);
	}

	public String getDescription() {
		if(description != null)
			return description.getText();
		return null;
	}
	public void setDescription(String description) {
		this.description.setText(description);
	}
}
