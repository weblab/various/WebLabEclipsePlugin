/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2012 Cassidian, an EADS company
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.wizard.plugin.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.PopupDialog;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Widget;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Table displaying Catalog Entries
 * @author Clément Caron - Cassidian
 */
public class CatalogEditor extends FieldEditor {

	private Table catalog;
	private Composite buttonBox;
	private SelectionListener selectionListener;
	private Button addButton;
	private Button editButton;
	private Button removeButton;
	private Button importButton;
	private Button exportButton;
	private final Shell shell;
	private PopupDialog dialog = null;
	private String popupText = null;

	/**
	 * Create the Editor
	 * @param name Preference Name, use to store into personnal settings of Eclipse
	 * @param labelText Label displayed on top of the Editor
	 * @param parent Container of the Editor
	 */
	public CatalogEditor(String name, String labelText, Composite parent) {
		init(name, labelText);
		createControl(parent);
		shell = parent.getShell();
	}

	/**
	 * Display entity information in a popup
	 * @param staredItem Entity to be displayed
	 */
	protected void displayPopupDialog(CatalogEntity staredItem) {
		
		popupText = "";
		if(staredItem.getArtifactId() != null && !staredItem.getArtifactId().equals("")) {
			popupText = popupText.concat(Constants.ARTIFACT_ID + " : " + staredItem.getArtifactId());
		}
		if(staredItem.getGroupId() != null && !staredItem.getGroupId().equals("")) {
			popupText = popupText.concat("\n" + Constants.GROUP_ID + " : " + staredItem.getGroupId());
		}
		if(staredItem.getVersion() != null && !staredItem.getVersion().equals("")) {
			popupText = popupText.concat("\n" + Constants.VERSION + " : " + staredItem.getVersion());
		}
		if(staredItem.getRepository() != null && !staredItem.getRepository().equals("")) {
			popupText = popupText.concat("\n" + Constants.REPOSITORY + " : " + staredItem.getRepository());
		}
		if(staredItem.getDescription() != null && !staredItem.getDescription().equals("")) {
			popupText = popupText.concat("\n" + Constants.DESCRIPTION + " : " + staredItem.getDescription());
		}
		
		dialog = new PopupDialog(shell,
				PopupDialog.HOVER_SHELLSTYLE,
				false,
				false,
				false,
				false,
				false,
				Constants.POPUP_DIALOG_TITLE,
				Constants.POPUP_DIALOG_INFO) {
					protected Point getInitialLocation(Point initialSize) {
						//show popup relative to cursor
						Display display = getShell().getDisplay();
						Point location = display.getCursorLocation();
						location.x += Constants.CURSOR_SIZE;
						location.y += Constants.CURSOR_SIZE;
						return location;
					}
					
					protected Control createDialogArea(Composite parent) {
						Label label = new Label(parent, SWT.WRAP);
						label.setText(popupText);
						label.addFocusListener(new FocusAdapter() {
							public void focusLost(FocusEvent event) {
								close();
							}
						});
						// Use the compact margins employed by PopupDialog.
						GridData gd = new GridData(GridData.BEGINNING
								| GridData.FILL_BOTH);
						gd.horizontalIndent = PopupDialog.POPUP_HORIZONTALSPACING;
						gd.verticalIndent = PopupDialog.POPUP_VERTICALSPACING;
						label.setLayoutData(gd);
						return label;
					}
			};
		dialog.open();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditor#adjustForNumColumns(int)
	 */
	@Override
	protected void adjustForNumColumns(int numColumns) {
		Control control = getLabelControl();
		((GridData) control.getLayoutData()).horizontalSpan = numColumns;
		((GridData) catalog.getLayoutData()).horizontalSpan = numColumns - 1;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditor#doFillIntoGrid(org.eclipse.swt.widgets.Composite, int)
	 */
	@Override
	protected void doFillIntoGrid(Composite parent, int numColumns) {
		Control control = getLabelControl(parent);
		GridData gd = new GridData();
		gd.horizontalSpan = numColumns;
		control.setLayoutData(gd);

		catalog = getTableControl(parent);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.verticalAlignment = GridData.FILL;
		gd.horizontalSpan = numColumns - 1;
		gd.grabExcessHorizontalSpace = true;
		gd.heightHint = 8 * catalog.getItemHeight() + catalog.getHeaderHeight();
		catalog.setLayoutData(gd);
		

		buttonBox = getButtonBoxControl(parent);
		gd = new GridData();
		gd.verticalAlignment = GridData.BEGINNING;
		buttonBox.setLayoutData(gd);
	}

	/**
	 * Create Buttons Composite
	 * @param parent Display Container
	 * @return the buttons Composite
	 */
	public Composite getButtonBoxControl(Composite parent) {
		if (buttonBox == null) {
			buttonBox = new Composite(parent, SWT.NULL);
			GridLayout layout = new GridLayout();
			layout.marginWidth = 0;
			buttonBox.setLayout(layout);
			createButtons(buttonBox);
			buttonBox.addDisposeListener(new DisposeListener() {
				public void widgetDisposed(DisposeEvent event) {
					addButton = null;
					editButton = null;
					removeButton = null;
					importButton = null;
					exportButton = null;
					buttonBox = null;
				}
			});

		} else {
			checkParent(buttonBox, parent);
		}

		selectionChanged();
		return buttonBox;
	}

	/**
	 * Change button option when selection change
	 */
	protected void selectionChanged() {
		int index = catalog.getSelectionIndex();
		int size = catalog.getItemCount();

		editButton.setEnabled(index >= 0);
		removeButton.setEnabled(index >= 0);
		exportButton.setEnabled(size > 0);
	}

	/**
	 * Create a Button
	 * @param parent Button Container
	 * @param key Button Label
	 * @return Button
	 */
	private Button createPushButton(Composite parent, String key) {
		Button button = new Button(parent, SWT.PUSH);
		button.setText(key);
		button.setFont(parent.getFont());
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		int widthHint = convertHorizontalDLUsToPixels(button,
				IDialogConstants.BUTTON_WIDTH);
		data.widthHint = Math.max(widthHint,
				button.computeSize(SWT.DEFAULT, SWT.DEFAULT, true).x);
		button.setLayoutData(data);
		button.addSelectionListener(getSelectionListener());
		return button;
	}

	/**
	 * Create the Buttons on the right of the Editor
	 * @param box Button Container
	 */
	private void createButtons(Composite box) {
		addButton = createPushButton(box, Constants.ADD_BUTTON_TEXT);
		editButton = createPushButton(box, Constants.EDIT_BUTTON_TEXT);
		removeButton = createPushButton(box, Constants.REMOVE_BUTTON_TEXT);
		importButton = createPushButton(box, Constants.IMPORT_BUTTON_TEXT);
		exportButton = createPushButton(box, Constants.EXPORT_BUTTON_TEXT);
	}

	/**
	 * Create the Entities Table
	 * @param parent Display Container
	 * @return the Table
	 */
	public Table getTableControl(Composite parent) {
		if (catalog == null) {
			catalog = new Table(parent, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL
					| SWT.H_SCROLL);

			TableColumn artifactIdColumn = new TableColumn(catalog, SWT.CENTER);
			TableColumn groupIdColumn = new TableColumn(catalog, SWT.CENTER);
			TableColumn versionColumn = new TableColumn(catalog, SWT.CENTER);
			artifactIdColumn.setWidth(Constants.ARTIFACT_ID_COLUMN_WIDTH);
			groupIdColumn.setWidth(Constants.GROUP_ID_COLUMN_WIDTH);
			versionColumn.setWidth(Constants.VERSION_COLUMN_WIDTH);
			artifactIdColumn.setText(Constants.ARTIFACT_ID);
			groupIdColumn.setText(Constants.GROUP_ID);
			versionColumn.setText(Constants.VERSION);
			catalog.setHeaderVisible(true);

			catalog.setFont(parent.getFont());
			
			catalog.addSelectionListener(getSelectionListener());
			catalog.addDisposeListener(new DisposeListener() {
				public void widgetDisposed(DisposeEvent event) {
					catalog = null;
				}
			});
			
			catalog.addMouseTrackListener(new MouseTrackListener() {
				
				@Override
				public void mouseHover(MouseEvent e) {
					if(dialog != null) {
						dialog.close();
						dialog = null;
					}
					float position = (catalog.getVerticalBar().getSelection() + e.y) / catalog.getItemHeight();
					int index = (int)position;
					if(index < catalog.getItemCount() && catalog.getItem(index).getData() instanceof CatalogEntity) {
						CatalogEntity staredItem = (CatalogEntity) catalog.getItem(index).getData();
						displayPopupDialog(staredItem);
					}
				}
				
				@Override
				public void mouseExit(MouseEvent e) {
					if(dialog != null) {
						dialog.close();
						dialog = null;
					}
				}
				
				@Override
				public void mouseEnter(MouseEvent e) { }
			});
		} else {
			checkParent(catalog, parent);
		}
		return catalog;
	}

	/**
	 * @return the Buttons selection listener
	 */
	private SelectionListener getSelectionListener() {
		if (selectionListener == null) {
			createSelectionListener();
		}
		return selectionListener;
	}

	/**
	 * Create Buttons Listener
	 */
	public void createSelectionListener() {
		selectionListener = new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				Widget widget = event.widget;
				if (widget == addButton) {
					addPressed();
				} else if (widget == editButton) {
					editPressed();
				} else if (widget == removeButton) {
					removePressed();
				} else if (widget == importButton) {
					importPressed();
				} else if (widget == exportButton) {
					exportPressed();
				} else if (widget == catalog) {
					selectionChanged();
				}
			}
		};
	}
	
	/**
	 * Create an Catalog Entity
	 * @param artifactId
	 * @param groupId
	 * @param version
	 * @param repository
	 * @param description
	 */
	public void createEntity(String artifactId, String groupId, String version, String repository, String description) {
		CatalogEntity entity = new CatalogEntity();
		entity.setArtifactId(artifactId);
		entity.setGroupId(groupId);
		entity.setVersion(version);
		entity.setRepository(repository);
		entity.setDescription(description);
		TableItem item = new TableItem(catalog, SWT.NULL);
		item.setText(new String[] {artifactId, groupId, version});
		item.setData(entity);
	}
	
	/**
	 * Actions when Add Button is pressed
	 */
	private void addPressed() {
		PopupCatalogEntity popup = new PopupCatalogEntity(this, shell);
		popup.openAddPopup();
	}

	/**
	 * Actions when Edit Button is pressed
	 */
	protected void editPressed() {
		if(catalog.getSelectionIndex() != -1
				&& catalog.getItem(catalog.getSelectionIndex()).getData() instanceof CatalogEntity) {
			TableItem item = catalog.getItem(catalog.getSelectionIndex());
			PopupCatalogEntity popup = new PopupCatalogEntity(this, shell);
			popup.openEditPopup(item);
		}
	}

	/**
	 * Actions when Import Button is pressed
	 */
	protected void importPressed() {
		FileDialog fileDialog = new FileDialog(shell, SWT.OPEN);
		String path = fileDialog.open();
		if(path != null && !path.equals("")) {
			try {
				importXml(new File(path));
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Actions when Export Button is pressed
	 */
	protected void exportPressed() {
		FileDialog fileDialog = new FileDialog(shell, SWT.SAVE);
		String path = fileDialog.open();
		if(path != null && !path.equals("")) {
			try {
				exportXml(new File(path));
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (TransformerException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Actions when Remove Button is pressed
	 */
	private void removePressed() {
		setPresentsDefaultValue(false);
		int index = catalog.getSelectionIndex();
		if (index >= 0) {
			catalog.remove(index);
			selectionChanged();
		}
	}

	/**
	 * Import Entities from a XML file
	 * @param importFile
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	private void importXml(File importFile) throws ParserConfigurationException, SAXException, IOException {
		Document dom;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		dom = db.parse(importFile);
		
		importXml(dom);
	}
	
	/**
	 * Import Entities from a Stream
	 * @param importFile
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	private void importXml(InputStream importFile) throws ParserConfigurationException, SAXException, IOException {
		Document dom;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		dom = db.parse(importFile);
		
		importXml(dom);
	}
	
	/**
	 * Import Entities from a DOM Document
	 * @param dom
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	private void importXml(Document dom) throws ParserConfigurationException, SAXException, IOException {
		NodeList archetypes = null;
		if(dom.getElementsByTagName(Constants.ARCHETYPES).getLength() > 0
				&& dom.getElementsByTagName(Constants.ARCHETYPES).item(0).hasChildNodes())
			archetypes = dom.getElementsByTagName(Constants.ARCHETYPES).item(0).getChildNodes();
		
		if(archetypes != null && archetypes.getLength() > 0) {
			for (int i = 0; i < archetypes.getLength(); i++) {
				Node archetype = archetypes.item(i);
				if(archetype.getChildNodes()!= null && archetype.getChildNodes().getLength() > 0){
					String group = "";
					String artifact = "";
					String version = "";
					String repository = "";
					String description = "";
					
					for (int j = 0; j < archetype.getChildNodes().getLength(); j++) {
						Node current = archetype.getChildNodes().item(j);
						if(current.getNodeName().equals(Constants.GROUP_ID) 
								&& current.getTextContent() != null
								&& !current.getTextContent().equals("null")) {
							group = current.getTextContent();
						}
						else if(current.getNodeName().equals(Constants.ARTIFACT_ID) 
								&& current.getTextContent() != null
								&& !current.getTextContent().equals("null")) {
							artifact = current.getTextContent();
						}
						else if(current.getNodeName().equals(Constants.VERSION) 
								&& current.getTextContent() != null
								&& !current.getTextContent().equals("null")) {
							version = current.getTextContent();
						}
						else if(current.getNodeName().equals(Constants.REPOSITORY) 
								&& current.getTextContent() != null
								&& !current.getTextContent().equals("null")) {
							repository = current.getTextContent();
						}
						else if(current.getNodeName().equals(Constants.DESCRIPTION) 
								&& current.getTextContent() != null
								&& !current.getTextContent().equals("null")) {
							description = current.getTextContent();
						}
					}
					
					if(artifact != null && !artifact.equals("")) {
						createEntity(artifact, group, version, repository, description);
					}
				}
			}
		}
	}
	
	/**
	 * Export Entities into a XML File
	 * @param exportFile
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws TransformerException
	 */
	private void exportXml(File exportFile) throws SAXException, IOException, ParserConfigurationException, TransformerException {
		if(exportFile.exists()) {
			if(!exportFile.delete())
				return;
		}
		
		Document dom;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		dom = db.newDocument();
		
		
		Element catalogRoot = dom.createElement(Constants.CATALOG);
		dom.appendChild(catalogRoot);
		
		Element archetype_catalog = dom.createElement(Constants.ARCHETYPE_CATALOG);
		catalogRoot.appendChild(archetype_catalog);
		
		Element archetypes = dom.createElement(Constants.ARCHETYPES);
		archetype_catalog.appendChild(archetypes);
		
		for (int i = 0; i < catalog.getItemCount(); i++) {
			if(catalog.getItem(i).getData() instanceof CatalogEntity) {
				CatalogEntity entity = (CatalogEntity) catalog.getItem(i).getData();
				Element archetype = dom.createElement(Constants.ARCHETYPE);
				archetypes.appendChild(archetype);
				
				if(entity.getArtifactId()!= null && !entity.getArtifactId().equals("")) {
					Element artifactId = dom.createElement(Constants.ARTIFACT_ID);
					artifactId.setTextContent(entity.getArtifactId());
					archetype.appendChild(artifactId);
				}
				
				if(entity.getGroupId()!= null && !entity.getGroupId().equals("")) {
					Element groupId = dom.createElement(Constants.GROUP_ID);
					groupId.setTextContent(entity.getGroupId());
					archetype.appendChild(groupId);
				}
				
				if(entity.getVersion()!= null && !entity.getVersion().equals("")) {
					Element version = dom.createElement(Constants.VERSION);
					version.setTextContent(entity.getVersion());
					archetype.appendChild(version);
				}
				
				if(entity.getRepository()!= null && !entity.getRepository().equals("")) {
					Element repository = dom.createElement(Constants.REPOSITORY);
					repository.setTextContent(entity.getRepository());
					archetype.appendChild(repository);
				}
				
				if(entity.getDescription()!= null && !entity.getDescription().equals("")) {
					Element description = dom.createElement(Constants.DESCRIPTION);
					description.setTextContent(entity.getDescription());
					archetype.appendChild(description);
				}
			}
		}
		
		StreamSource style = new StreamSource(getClass().getResource(Constants.INDENT_XSLT).openStream());
		TransformerFactory tFactory = TransformerFactory.newInstance();
		Transformer transformer = tFactory.newTransformer(style);
		
		DOMSource source = new DOMSource(dom);
//		StreamResult result = new StreamResult(System.out);
		StreamResult result = new StreamResult(exportFile);
		transformer.transform(source, result); 
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditor#doLoad()
	 */
	@Override
	protected void doLoad() {
		if (catalog != null) {
            String s = getPreferenceStore().getString(getPreferenceName());
            List<String> entityList = parseString(s, Constants.SERIALISATION_LINE_SEPARATOR);
            for (int i = 0; i < entityList.size(); i++) {
            	List<String> valueList = parseString(entityList.get(i), Constants.SERIALISATION_FIELD_SEPARATOR);
            	if(valueList.size() == 5) {
            		createEntity(valueList.get(0), valueList.get(1), valueList.get(2), valueList.get(3), valueList.get(4));
            	}
            }
        }
		selectionChanged();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditor#doLoadDefault()
	 */
	@Override
	protected void doLoadDefault() {
		catalog.removeAll();
		try {
			importXml(getClass().getResource(Constants.CATALOG_XML).openStream());
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		selectionChanged();
		setPresentsDefaultValue(false);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditor#doStore()
	 */
	@Override
	protected void doStore() {
		String s = createList(catalog.getItems());
        if (s != null) {
			getPreferenceStore().setValue(getPreferenceName(), s);
		}
	}
	
	/**
	 * Parse a String into a List of String contained between each Separator
	 * @param stringList the String to be parse
	 * @param separator the Separator String
	 * @return the List of String
	 */
	protected List<String> parseString(String stringList, String separator) {
        List<String> list = new ArrayList<String>();
        
        int index = 0;
        int oldIndex = 0;
        while((index = stringList.indexOf(separator, index)) != -1) {
        	list.add(stringList.substring(oldIndex, index));
        	index++;
        	oldIndex = index;
        }
        list.add(stringList.substring(oldIndex));
        return list;
    }
	
	/**
	 * Create a String from the Table Entities
	 * @param items Table Entities
	 * @return the String
	 */
	protected String createList(TableItem[] items) {
        StringBuffer path = new StringBuffer("");

        for (int i = 0; i < items.length; i++) {
        	if(items[i].getData() instanceof CatalogEntity) {
        		CatalogEntity entity = (CatalogEntity) items[i].getData();
        		path.append(entity.getArtifactId() + Constants.SERIALISATION_FIELD_SEPARATOR);
        		path.append(entity.getGroupId() + Constants.SERIALISATION_FIELD_SEPARATOR);
        		path.append(entity.getVersion() + Constants.SERIALISATION_FIELD_SEPARATOR);
        		path.append(entity.getRepository() + Constants.SERIALISATION_FIELD_SEPARATOR);
        		path.append(entity.getDescription() + Constants.SERIALISATION_LINE_SEPARATOR);
        	}
        }
        return path.toString();
    }

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditor#getNumberOfControls()
	 */
	@Override
	public int getNumberOfControls() {
		return 2;
	}
}
