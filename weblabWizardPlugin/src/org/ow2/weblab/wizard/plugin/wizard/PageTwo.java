/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2012 Cassidian, an EADS company
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.wizard.plugin.wizard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.ow2.weblab.wizard.plugin.utils.CatalogEntity;
import org.ow2.weblab.wizard.plugin.utils.Constants;
import org.ow2.weblab.wizard.plugin.utils.WebLabEntity;


/**
 * Page Two of the Wizard
 * @author Clément Caron - Cassidian
 */
public class PageTwo extends WizardPage {
	private Text groupId, artifactId, serviceName;
	private Label labelServiceName;
	private Composite container;
	private List<CatalogEntity> catalog;
	private CatalogEntity archetypeChosen = null;
	private Combo archetypesCombo;
	private Label description;
	private List<String> webLabInterfaces = null;
	private List<String> webLabInterfacesDeprecated = null;
	private Map<String, Button> webLabCheckBoxes = null;

	/**
	 * Constructor - Create The Page and set the Catalog
	 * @param catalog
	 */
	public PageTwo(List<CatalogEntity> catalog) {
		super(Constants.PAGE_TWO_NAME);
		setTitle(Constants.PAGE_TWO_TITLE);
		setDescription(Constants.PAGE_TWO_DESCRIPTION);
		this.catalog = catalog;
		archetypeChosen = catalog.get(0);
		initWebLabInterfaces();
	}

	private void initWebLabInterfaces() {
		webLabInterfaces = new ArrayList<String>();
		webLabInterfaces.add("analyser");
		webLabInterfaces.add("auditable");
		webLabInterfaces.add("configurable");
		webLabInterfaces.add("cleanable");
		webLabInterfaces.add("indexer");
		webLabInterfaces.add("queuemanager");
		webLabInterfaces.add("reportprovider");
		webLabInterfaces.add("resourcecontainer");
		webLabInterfaces.add("searcher");
		webLabInterfaces.add("sourcereader");
		webLabInterfaces.add("trainable");
		
		webLabInterfacesDeprecated = new ArrayList<String>();
		webLabInterfacesDeprecated.add("reportprovider");
		webLabInterfacesDeprecated.add("sourcereader");
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 2;
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		
		Label labelArchetype = new Label(container, SWT.NULL);
		labelArchetype.setText("Select Archetype");
		
		
		String[] comboList = buildArchetypesCombo();
		archetypesCombo = new Combo(container, SWT.BORDER | SWT.SINGLE | SWT.READ_ONLY);
		archetypesCombo.setLayoutData(gd);
		archetypesCombo.setItems(comboList);
		archetypesCombo.select(0);
		archetypesCombo.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				archetypeChosen = catalog.get(archetypesCombo.getSelectionIndex());
				if(archetypeChosen instanceof WebLabEntity) {
					if(labelServiceName != null) {
						labelServiceName.setEnabled(true);
					}
					if(serviceName != null) {
						serviceName.setEnabled(true);
					}
					if(webLabCheckBoxes != null) {
						for (int i = 0; i < webLabInterfaces.size(); i++) {
							webLabCheckBoxes.get(webLabInterfaces.get(i)).setEnabled(true);
						}
					}
				}
				else {
					if(labelServiceName != null) {
						labelServiceName.setEnabled(false);
					}
					if(serviceName != null) {
						serviceName.setEnabled(false);
					}
					if(webLabCheckBoxes != null) {
						for (int i = 0; i < webLabInterfaces.size(); i++) {
							webLabCheckBoxes.get(webLabInterfaces.get(i)).setEnabled(false);
						}
					}
				}
				fillDescriptionBox(archetypeChosen);
				testPageComplete();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) { }
		});
		
		
		Composite checkBoxesContainer = new Composite(container, SWT.NULL);
		GridData checkBoxesGridData = new GridData(GridData.FILL_HORIZONTAL);
		checkBoxesGridData.horizontalSpan = 2;
		checkBoxesContainer.setLayoutData(checkBoxesGridData);
		GridLayout checkBoxesLayout = new GridLayout();
		checkBoxesLayout.numColumns = 3;
		checkBoxesContainer.setLayout(checkBoxesLayout);
		webLabCheckBoxes = new HashMap<String, Button>();
		for (int i = 0; i < webLabInterfaces.size(); i++) {
			Button button = new Button(checkBoxesContainer, SWT.CHECK);
			String iface = webLabInterfaces.get(i);
			button.setText(iface);
			if(webLabInterfacesDeprecated.contains(iface)) {
				button.setForeground(new Color(null, 255, 0, 0));
			}
			GridData gd2 = new GridData(GridData.FILL_HORIZONTAL);
			button.setLayoutData(gd2);
			button.addSelectionListener(new SelectionListener() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					testPageComplete();
				}
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
				}
			});
			webLabCheckBoxes.put(iface, button);
		}
		
		GridData warningGridData = new GridData(GridData.FILL_HORIZONTAL);
		warningGridData.horizontalSpan = 2;
		Label warning = new Label(container, SWT.CENTER);
		warning.setLayoutData(warningGridData);
		warning.setText("red interfaces are deprecated");
		warning.setForeground(new Color(null, 255, 0, 0));
		
		
		labelServiceName = new Label(container, SWT.NULL);
		labelServiceName.setText("Service Name");
		
		serviceName = new Text(container, SWT.BORDER | SWT.SINGLE);
		serviceName.setText("");
		serviceName.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				testPageComplete();
			}

		});
		serviceName.setLayoutData(gd);
		
		Label labelGroupId = new Label(container, SWT.NULL);
		labelGroupId.setText("GroupId");
		
		groupId = new Text(container, SWT.BORDER | SWT.SINGLE);
		groupId.setText("");
		groupId.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				testPageComplete();
			}

		});
		groupId.setLayoutData(gd);
		
		Label labelArtifactId = new Label(container, SWT.NULL);
		labelArtifactId.setText("ArtifactId");
		
		artifactId = new Text(container, SWT.BORDER | SWT.SINGLE);
		artifactId.setText("");
		artifactId.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				testPageComplete();
			}

		});
		artifactId.setLayoutData(gd);
		
		description = new Label(container, SWT.BORDER | SWT.WRAP);
		description.setText(Constants.DESCRIPTION_STARTER);
		GridData gd2 = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
		gd2.horizontalSpan = 2;
		gd2.minimumHeight = 125;
		description.setLayoutData(gd2);
		
		fillDescriptionBox(archetypeChosen);
		
		// Required to avoid an error in the system
		setControl(container);
		setPageComplete(false);

	}
	
	/**
	 * Fill the Description Box with Selected Entity information
	 * @param archetype
	 */
	protected void fillDescriptionBox(CatalogEntity archetype) {
		String descriptionString = "";
		descriptionString = descriptionString.concat(Constants.DESCRIPTION_STARTER);
		
		if(archetype instanceof WebLabEntity) {
			descriptionString = descriptionString.concat("\n" + Constants.WEBLAB_COMBO_DESC);
		}
		else {
			if(archetype.getGroupId()!= null) {
				descriptionString = descriptionString.concat("\n" + Constants.GROUP_ID + " : " + archetype.getGroupId());
			}
			if(archetype.getArtifactId()!= null) {
				descriptionString = descriptionString.concat("\n" + Constants.ARTIFACT_ID + " : " + archetype.getArtifactId());
			}
			if(archetype.getVersion()!= null) {
				descriptionString = descriptionString.concat("\n" + Constants.VERSION + " : " + archetype.getVersion());
			}
			if(archetype.getRepository()!= null) {
				descriptionString = descriptionString.concat("\n" + Constants.REPOSITORY + " : " + archetype.getRepository());
			}
			if(archetype.getDescription()!= null) {
				descriptionString = descriptionString.concat("\n" + Constants.DESCRIPTION + " : " + archetype.getDescription());
			}
		}
		
		description.setText(descriptionString);
	}

	/**
	 * Test if the page is completed
	 */
	protected void testPageComplete() {
		boolean result = true;
		int descriptionSet = 0;//0 all good; 1 warning; 2 error
		setErrorMessage(null);
		setMessage(null);
//		setMessage(newMessage, newType)
		int checkbox = testWebLabBoxesChosen();
		
		if(!groupId.getText().isEmpty()
				&& !artifactId.getText().isEmpty()
				&& archetypeChosen != null
				&& (!(archetypeChosen instanceof WebLabEntity)
				|| (archetypeChosen instanceof WebLabEntity
						&& !serviceName.getText().isEmpty()
						&& checkbox != 2))) {
			
			
			if(!artifactId.getText().matches(Constants.REGEX_ERROR_ARTIFACT)) {
				result = false;
				if(descriptionSet <= 1) {
					setErrorMessage("ArtifactId field is not correct");
					descriptionSet = 2;
				}
			}
			
			if(serviceName.getText().matches(Constants.REGEX_ERROR_CLASSNAME)) {
				if(!serviceName.getText().matches(Constants.REGEX_WARN_CLASSNAME)) {
					if(descriptionSet == 0) {
						setMessage("ServiceName does not fulfilled the java convention", WizardPage.WARNING);
						descriptionSet = 1;
					}
				}
			}
			else {
				result = false;
				if(descriptionSet <= 1) {
					setErrorMessage("ServiceName field is not correct");
					descriptionSet = 2;
				}
			}
			
			if(groupId.getText().matches(Constants.REGEX_ERROR_PACKAGE)) {
				if(!groupId.getText().matches(Constants.REGEX_WARN_PACKAGE)) {
					if(descriptionSet == 0) {
						setMessage("GroupId does not fulfilled the java convention", WizardPage.WARNING);
						descriptionSet = 1;
					}
				}
			}
			else {
				result = false;
				if(descriptionSet <= 1) {
					setErrorMessage("GroupId field is not correct");
					descriptionSet = 2;
				}
			}
			
			if(checkbox == 1 && descriptionSet == 0) {
				setMessage("You have selected one or several deprecated service(s)", WizardPage.WARNING);
				descriptionSet = 1;
			}
			
		}
		else {
			result = false;
			if(descriptionSet <= 1) {
				setErrorMessage("Fields must be filled");
				descriptionSet = 2;
			}
		}
		
//		if(descriptionSet == 0) {
//			setDescription(Constants.PAGE_TWO_DESCRIPTION);
//		}
		setPageComplete(result);
	}
	
	/**
	 * @return Test the check Boxes
	 */
	private int testWebLabBoxesChosen() {
		int result = 2;
		
		for (int i = 0; i < webLabInterfaces.size(); i++) {
			if(webLabCheckBoxes.get(webLabInterfaces.get(i)).getSelection()) {
				if(webLabInterfacesDeprecated.contains(webLabInterfaces.get(i))) {
					result = 1;
				}
				else {
					if(result == 2) {
						result = 0;
					}
				}
			}
		}
		
		return result;
	}

	/**
	 * Create the Catalog Combo Box
	 * @return a String Array for The Combo Box
	 */
	private String[] buildArchetypesCombo() {
		List<String> comboList = new LinkedList<String>();
		for (int i = 0; i < catalog.size(); i++) {
			if(catalog.get(i) instanceof WebLabEntity) {
				comboList.add(Constants.WEBLAB_COMBO_NAME);
			}
			else {
				CatalogEntity archetype = catalog.get(i);
				if(archetype.getArtifactId() != null && !archetype.getArtifactId().equals("")) {
					String value = "";
					if(archetype.getGroupId() != null && !archetype.getGroupId().equals("")) {
						value = value.concat(archetype.getGroupId() + " | ");
					}
					value = value.concat(archetype.getArtifactId());
					if(archetype.getVersion() != null && !archetype.getVersion().equals("")) {
						value = value.concat(" | " + archetype.getVersion());
					}
					comboList.add(value);
				}
			}
		}
		return comboList.toArray(new String[0]);
	}
	
	

	/**
	 * @return the Project Name
	 */
	public String getProjectName() {
		if(serviceName.getText() != null && !serviceName.getText().equals(""))
			return serviceName.getText();
		else
			return artifactId.getText();
	}
	
	/**
	 * @return the Service Name
	 */
	public String getServiceName() {
		return serviceName.getText();
	}
	
	/**
	 * @return the GroupId
	 */
	public String getGroupId() {
		return groupId.getText();
	}
	
	/**
	 * @return the ArtifactId
	 */
	public String getArtifactId() {
		return artifactId.getText();
	}
	
	/**
	 * @return the Entity Chosen of the Combo Box
	 */
	public CatalogEntity getArchetypeChosen() {
		return archetypeChosen;
	}
	
	/**
	 * @return the Entities Chosen of the Check Boxes
	 */
	public String getWebLabBoxesChosen() {
		String result = null;
		
		for (int i = 0; i < webLabInterfaces.size(); i++) {
			if(webLabCheckBoxes.get(webLabInterfaces.get(i)).getSelection()) {
				if(result != null) {
					result = result.concat("," + webLabInterfaces.get(i));
				}
				else {
					result = webLabInterfaces.get(i);
				}
			}
		}
		
		return result;
	}
}
