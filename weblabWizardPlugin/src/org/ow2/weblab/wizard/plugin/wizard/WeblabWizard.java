/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2012 Cassidian, an EADS company
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.wizard.plugin.wizard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.ow2.weblab.wizard.plugin.Activator;
import org.ow2.weblab.wizard.plugin.utils.CatalogEntity;
import org.ow2.weblab.wizard.plugin.utils.Constants;
import org.ow2.weblab.wizard.plugin.utils.PopupDialog;
import org.ow2.weblab.wizard.plugin.utils.WebLabEntity;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


/**
 * The WebLab Wizard Main Class
 * @author Clément Caron - Cassidian
 */
public class WeblabWizard extends Wizard implements INewWizard {

	private PageOne one;
	private PageTwo two;
	private List<CatalogEntity> catalog = null;
	private ILog log = Activator.getDefault().getLog();
	private IStatus status;
	private ProcessBuilder procBuilder;
	private Process proc;
	private String message;
	protected IProgressMonitor thisMonitor;

	/**
	 * Constructor - Create the Wizard
	 */
	public WeblabWizard() {
		super();
		setNeedsProgressMonitor(true);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages() {
		one = new PageOne();
		two = new PageTwo(catalog);
		addPage(one);
		addPage(two);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		final String projectName = two.getProjectName();
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor)
					throws InvocationTargetException {
				try {
					doFinish(projectName, monitor);
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
		};
		try {
			getContainer().run(false, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			PopupDialog.openError(getShell(), "Error", "Core Exception", e.getMessage());
			return false;
		}
		
		return true;
	}

	/**
	 * Perform Maven Project Creation
	 * @param projectName
	 * @param monitor
	 * @throws CoreException
	 */
	private void doFinish(String projectName, IProgressMonitor monitor)
			throws CoreException {
		this.thisMonitor = monitor;
		boolean isJavaWorking = isJavaWorking();
		String javaHome = getJavaHome();
		boolean mvnWorked = false;
		monitor.beginTask("creating Maven Project", 100);
		monitor.worked(1);
		if(Platform.getLocation().append(two.getArtifactId()).toFile().exists()) {
			MessageDialog.openError(getShell(), "Error", "Folder " + two.getArtifactId() + " already exists");
		}
		else {
			List<String> command;
			String path = Activator.getDefault().getPreferenceStore().getString(Constants.MVN_PATH);
			String settings = Activator.getDefault().getPreferenceStore().getString(Constants.MVN_SETTINGS);
			String mavenOptions = Activator.getDefault().getPreferenceStore().getString(Constants.MVN_MAVEN_OPTS);
			String createOptions = Activator.getDefault().getPreferenceStore().getString(Constants.MVN_CREATE_OPTS);
			String eclipseOptions = Activator.getDefault().getPreferenceStore().getString(Constants.MVN_ECLIPSE_OPTS);
			if(path != null && !path.equals(""))
				command = createCommand(path, settings, createOptions);
			else
				command = createCommand(Constants.MVN, settings, createOptions);
			
			status = new Status(IStatus.INFO, Activator.PLUGIN_ID, "JAVA_HOME set to " + javaHome);
			log.log(status);
			
			procBuilder = new ProcessBuilder(command);
			Map<String, String> env = procBuilder.environment();
			env.put("JAVA_HOME", javaHome);
			
			if(!isJavaWorking) {
				String systemPath = env.get("PATH");
				systemPath = javaHome.concat(File.separator).concat("bin").concat(File.pathSeparator).concat(systemPath);
				status = new Status(IStatus.INFO, Activator.PLUGIN_ID, "PATH updated : " + systemPath);
				log.log(status);
				env.remove("PATH");
				env.put("PATH", systemPath);
			}
			
			if(mavenOptions != null && !mavenOptions.equals("")) {
				status = new Status(IStatus.INFO, Activator.PLUGIN_ID, "Adding env Variables:\nMAVEN_OPTS: " + mavenOptions);
				log.log(status);
				env.put("MAVEN_OPTS", mavenOptions);
			}
			
			status = new Status(IStatus.INFO, Activator.PLUGIN_ID, "Starting Maven Process");
			log.log(status);
			File folder = Platform.getLocation().toFile();
			if(!folder.exists()) {
				folder.mkdirs();
			}
			procBuilder.directory(folder);
			status = new Status(IStatus.INFO, Activator.PLUGIN_ID, "Maven Command:\n" + procBuilder.command() + " in " + procBuilder.directory());
			log.log(status);
			
			
			
			Display.getDefault().syncExec(new Runnable() {
				@Override
				public void run() {
					try {
						proc = procBuilder.start();
						
						InputStream stream = proc.getInputStream();
						BufferedReader br = new BufferedReader(new InputStreamReader(stream));
						String line = "";
						
						message = "";
					
						int percent = 58;
						while((line = br.readLine()) != null) {
							message = message.concat(line + "\n");
							
							if(percent > 0) {
								thisMonitor.worked(1);
								percent--;
							}
						}
						
						proc.waitFor();
						
						status = new Status(IStatus.INFO, Activator.PLUGIN_ID, message);
						log.log(status);
						
						int work = 1 + percent;
						thisMonitor.worked(work);
					} catch (Exception e) {
						MessageDialog.openError(getShell(), "Error", Constants.MVN_ERROR);
						e.printStackTrace();
					}
				}
			});
			
			if(proc.exitValue() != 0) {
				if(Platform.getLocation().append(two.getArtifactId()).toFile().exists()) {
					PopupDialog.openError(getShell(), "Error", "Something went wrong during the maven command, a folder " + two.getArtifactId() + " remains in the workspace", message);
				}
				else {
					PopupDialog.openError(getShell(), "Error", "Something went wrong during the maven command", message);
				}
			}
			else {
				if(Platform.getLocation().append(two.getArtifactId()).toFile().exists()) {
					List<String> eclipseCommand = new ArrayList<String>();
					
					if(path != null && !path.equals(""))
						eclipseCommand.add(path);
					else
						eclipseCommand.add(Constants.MVN);
					
					eclipseCommand.add("eclipse:eclipse");
					
					if(settings != null && !settings.equals("")) {
						eclipseCommand.add("--settings");
						eclipseCommand.add(settings);
					}
					else {
						try {
							File set = createTempSettings();// dirty, but only way to have absolute path to the settings file
							eclipseCommand.add("--settings");
							eclipseCommand.add(set.getAbsolutePath());
						} catch (IOException e) {
							PopupDialog.openWarning(getShell(), "Error", "Problem while generating settings.xml", e.getMessage());
						}
					}
					
					if(eclipseOptions != null && !eclipseOptions.equals("")) {
						eclipseCommand.add(eclipseOptions);
					}
					
					procBuilder = new ProcessBuilder(eclipseCommand);
					env = procBuilder.environment();
					env.put("JAVA_HOME", javaHome);
					
					if(!isJavaWorking) {
						String systemPath = env.get("PATH");
						systemPath = javaHome.concat(System.getProperty("file.separator")).concat("bin:").concat(systemPath);
						env.remove("PATH");
						env.put("PATH", systemPath);
					}
					
					if(mavenOptions != null && !mavenOptions.equals("")) {
						status = new Status(IStatus.INFO, Activator.PLUGIN_ID, "Adding env Variables:\nMAVEN_OPTS: " + mavenOptions);
						log.log(status);
						env.put("MAVEN_OPTS", mavenOptions);
					}
					
					procBuilder.directory(Platform.getLocation().append(two.getArtifactId()).toFile());
					status = new Status(IStatus.INFO, Activator.PLUGIN_ID, "Eclipse Command:\n" + procBuilder.command() + " in " + procBuilder.directory());
					log.log(status);
					
					
					
					
					
					Display.getDefault().syncExec(new Runnable() {
						@Override
						public void run() {
							try {
								proc = procBuilder.start();
								
								InputStream stream = proc.getInputStream();
								BufferedReader br = new BufferedReader(new InputStreamReader(stream));
								String line = "";
								
								message = "";
							
								int percent = 38;
								while((line = br.readLine()) != null) {
									message = message.concat(line + "\n");
									
									if(percent > 0) {
										thisMonitor.worked(1);
										percent--;
									}
								}
								
								proc.waitFor();
								
								status = new Status(IStatus.INFO, Activator.PLUGIN_ID, message);
								log.log(status);
							} catch (Exception e) {
								MessageDialog.openError(getShell(), "Error", Constants.MVN_ERROR);
								e.printStackTrace();
							}
						}
					});
					
					if(proc.exitValue() != 0) {
						PopupDialog.openWarning(getShell(), "Warning", "Something went wrong during the mvn eclipse:eclipse command, continuing process...", message);
					}
					mvnWorked = true;
					}
				else {
					PopupDialog.openError(getShell(), "Error", "Something went wrong during the maven command: Can't find maven folder\nThis could come from a MAVEN_OPTS problem", message);
				}
			}
		}
		monitor.done();
		
		if(mvnWorked) {
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			IProject project = root.getProject(two.getArtifactId());
			if (!project.exists())
				project.create(monitor);
			if (!project.isOpen())
				project.open(monitor);
		}
	}

	private boolean isJavaWorking() {
		boolean result = false;
		
		try {
			List<String> command = new ArrayList<String>();
			
			command.add("java");
			command.add("-version");
			
			ProcessBuilder javaTest = new ProcessBuilder(command);
		
			Process proc = javaTest.start();
			
			InputStream stream = proc.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(stream));
			String line = "";
			
			String message = "";
			while((line = br.readLine()) != null) {
				message = message.concat(line + "\n");
			}
			
			
			proc.waitFor();
			
			if(proc.exitValue() == 0) {
				result = true;
			}
		} 
		catch (Exception e) { }
		
		return result;
	}

	private String getJavaHome() {
		if(System.getenv("JAVA_HOME") == null) {
			return(JavaRuntime.getDefaultVMInstall().getInstallLocation().getAbsolutePath());
		}
		else {
			return(System.getenv("JAVA_HOME"));
		}
	}

	private List<String> createCommand(String path, String settings, String createOptions) {
		List<String> command = new LinkedList<String>();
		
		command.add(path);
		if(settings != null && !settings.equals("")) {
			command.add("--settings");
			command.add(settings);
		}
		else {
			try {
				File set = createTempSettings();// dirty, but only way to have absolute path to the settings file
				command.add("--settings");
				command.add(set.getAbsolutePath());
			} catch (IOException e) {
				PopupDialog.openWarning(getShell(), "Error", "Problem while generating settings.xml", e.getMessage());
			}
		}
		
//		command.add("-X");
		
		if(two.getArchetypeChosen() instanceof WebLabEntity) {
			command.add(Constants.MVN_CREATE_WEBLAB_ARCHETYPE);
			command.add(Constants.MVN_GROUP_ID + two.getGroupId());
			command.add(Constants.MVN_ARTIFACT_ID + two.getArtifactId());
			command.add(Constants.MVN_SERVICE_NAME + two.getServiceName());
			command.add(Constants.MVN_WEBLAB_INTERFACE + two.getWebLabBoxesChosen());
//			command.add(Constants.MVN_WEBLAB_FIX_COMMAND);
			if(createOptions != null && !createOptions.equals("")) {
				command.add(createOptions);
			}
		}
		else {
			command.add(Constants.MVN_CREATE_ARCHETYPE);
			command.add(Constants.MVN_GROUP_ID + two.getGroupId());
			command.add(Constants.MVN_ARTIFACT_ID + two.getArtifactId());
			command.add(Constants.MVN_ARCHETYPE_ARTIFACT_ID + two.getArchetypeChosen().getArtifactId());
			if(createOptions != null && !createOptions.equals("")) {
				command.add(createOptions);
			}
		}
		
		
		return command;
	}

	private File createTempSettings() throws IOException {
		File result = File.createTempFile(Constants.PREFIX_SETTINGS, Constants.SUFFIX_SETTINGS);
		InputStream in = getClass().getResource(Constants.DEFAULT_SETTINGS).openStream();
		StringWriter sWriter = new StringWriter();
	    FileWriter out = new FileWriter(result);
	    int c;

	    while ((c = in.read()) != -1)
	    	sWriter.write(c);

	    in.close();
	    sWriter.close();
	    
	    String str = sWriter.toString();
	    
	    if (JavaCore.getClasspathVariable("M2_REPO") != null
				&& !JavaCore.getClasspathVariable("M2_REPO").isEmpty()) {
	    	String settingsItem = "<!-- localRepository -->";
		    String localRepo = "<localRepository>" + JavaCore.getClasspathVariable("M2_REPO") + "</localRepository>";
		    str = str.replace(settingsItem, localRepo);
	    }
	    
	    out.write(str);
	    out.close();
	    
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
	 */
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		
		this.catalog = new LinkedList<CatalogEntity>();
		this.catalog.add(new WebLabEntity());
		List<CatalogEntity> cat = parsePreferencesCatalog();
		
		if(cat != null) {
			this.catalog.addAll(cat);
		}
	}
	
	/**
	 * Parse a String into a List of String contained between each Separator
	 * @param stringList the String to be parse
	 * @param separator the Separator String
	 * @return the List of String
	 */
	private List<String> parseString(String stringList, String separator) {
        List<String> list = new ArrayList<String>();
        
        int index = 0;
        int oldIndex = 0;
        while((index = stringList.indexOf(separator, index)) != -1) {
        	list.add(stringList.substring(oldIndex, index));
        	index++;
        	oldIndex = index;
        }
        list.add(stringList.substring(oldIndex));
        return list;
    }
	
	/**
	 * Parse the Preference Catalog to build the Entities
	 * @return a List of Entities
	 */
	private List<CatalogEntity> parsePreferencesCatalog() {
		String s = Activator.getDefault().getPreferenceStore().getString(Constants.CATALOG_SETTINGS);
		if(s != null && !s.equals("")) {
	        List<String> entityList = parseString(s, Constants.SERIALISATION_LINE_SEPARATOR);
	        List<CatalogEntity> archetypeList = new LinkedList<CatalogEntity>();
	        
	        for (int i = 0; i < entityList.size(); i++) {
	        	List<String> valueList = parseString(entityList.get(i), Constants.SERIALISATION_FIELD_SEPARATOR);
	        	if(valueList.size() == 5) {
	        		CatalogEntity arch = new CatalogEntity();
					
					arch.setArtifactId(valueList.get(0));
					arch.setGroupId(valueList.get(1));
					arch.setVersion(valueList.get(2));
					arch.setRepository(valueList.get(3));
					arch.setDescription(valueList.get(4));
					
					archetypeList.add(arch);
	        	}
	        }
	        
			if(!archetypeList.isEmpty())
				return archetypeList;
		}
		return null;
	}

	/**
	 * Parse the Catalog.xml to build the default Entities
	 * @return a List of Entities
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	@SuppressWarnings("unused")
	@Deprecated
	private List<CatalogEntity> parseXMLCatalog() throws ParserConfigurationException, SAXException, IOException {
		List<CatalogEntity> archetypeList = new LinkedList<CatalogEntity>();
		
		Document dom;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		dom = db.parse(getClass().getResource(Constants.CATALOG_XML).openStream());
		
		NodeList archetypes;
		if(dom.getElementsByTagName(Constants.ARCHETYPES).getLength()>0
				&& dom.getElementsByTagName(Constants.ARCHETYPES).item(0).hasChildNodes())
			archetypes = dom.getElementsByTagName(Constants.ARCHETYPES).item(0).getChildNodes();
		else
			return archetypeList;
		
		
		if(archetypes != null && archetypes.getLength() > 0) {
			for (int i = 0; i < archetypes.getLength(); i++) {
				Node archetype = archetypes.item(i);
				if(archetype.getChildNodes()!= null && archetype.getChildNodes().getLength() > 0){
					String group = null;
					String artifact = null;
					String version = null;
					String repository = null;
					String description = null;
					
					for (int j = 0; j < archetype.getChildNodes().getLength(); j++) {
						Node current = archetype.getChildNodes().item(j);
						if(current.getNodeName().equals(Constants.GROUP_ID)) {
							group = current.getTextContent();
						}
						else if(current.getNodeName().equals(Constants.ARTIFACT_ID)) {
							artifact = current.getTextContent();
						}
						else if(current.getNodeName().equals(Constants.VERSION)) {
							version = current.getTextContent();
						}
						else if(current.getNodeName().equals(Constants.REPOSITORY)) {
							repository = current.getTextContent();
						}
						else if(current.getNodeName().equals(Constants.DESCRIPTION)) {
							description = current.getTextContent();
						}
					}
					
					if(artifact != null && !artifact.equals("")) {
						CatalogEntity arch = new CatalogEntity();
						arch.setGroupId(group);
						arch.setArtifactId(artifact);
						arch.setVersion(version);
						arch.setRepository(repository);
						arch.setDescription(description);
						
						archetypeList.add(arch);
					}
				}
			}
		}
		return archetypeList;
	}
}