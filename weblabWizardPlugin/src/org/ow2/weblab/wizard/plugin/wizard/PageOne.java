/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2012 Cassidian, an EADS company
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.wizard.plugin.wizard;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.ow2.weblab.wizard.plugin.Activator;
import org.ow2.weblab.wizard.plugin.utils.Constants;

/**
 * First Page of the Wizard
 * 
 * @author Clément Caron - Cassidian
 */
public class PageOne extends WizardPage {
	private Composite container;
	private Image image = null;
	private Label imageLabel;

	private Link messageLink;
	private Label preMessage, postMessage;
	private Label repoLabel;
	private FileDialog fileDialog;
	private Button browse;
	private boolean noProblem = true;

	/**
	 * Constructor - Create the Page, display a logo and test Maven Settings
	 */
	public PageOne() {
		super(Constants.PAGE_ONE_NAME);
		setTitle(Constants.PAGE_ONE_TITLE);
		setDescription(Constants.PAGE_ONE_DESCRIPTION);
		try {
			image = new Image(null, getClass().getResource(
					Constants.WEBLAB_LOGO).openStream());
		} catch (IOException e) {
			e.printStackTrace();
		}

		String path = Activator.getDefault().getPreferenceStore()
				.getString(Constants.MVN_PATH);

		if (path != null && !path.equals(""))
			noProblem = testMaven(path);
		else
			noProblem = testMaven(Constants.MVN);
	}

	/**
	 * Test Maven Settings
	 * 
	 * @param path
	 *            Path for Maven
	 * @return true if Maven is correctly set, false otherwise
	 */
	private boolean testMaven(String path) {
		try {
			Process proc = Runtime.getRuntime().exec(path + Constants.MVN_TEST);
			proc.waitFor();
			if (proc.exitValue() == 0) {
				return true;
			}
		} catch (IOException e) {
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);

		imageLabel = new Label(container, SWT.CENTER);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		imageLabel.setLayoutData(gd);
		imageLabel.setImage(image);

		if (!noProblem) {
			preMessage = new Label(container, SWT.CENTER);
			GridData gdPreMessage = new GridData(
					GridData.HORIZONTAL_ALIGN_CENTER);
			preMessage.setLayoutData(gdPreMessage);
			preMessage.setText(Constants.MVN_NOT_FOUND);
			preMessage.setForeground(new Color(null, 255, 0, 0));

			messageLink = new Link(container, SWT.CENTER);
			GridData gdMessage = new GridData(GridData.HORIZONTAL_ALIGN_CENTER);
			messageLink.setLayoutData(gdMessage);
			messageLink.setForeground(new Color(null, 255, 0, 0));
			messageLink.setText(Constants.MVN_NOT_FOUND2);
			messageLink.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					try {
						PlatformUI.getWorkbench().getBrowserSupport()
								.getExternalBrowser()
								.openURL(new URL("http://maven.apache.org/"));
					} catch (PartInitException ex) {
						ex.printStackTrace();
					} catch (MalformedURLException ex) {
						ex.printStackTrace();
					}
				}
			});

			postMessage = new Label(container, SWT.CENTER);
			GridData gdPostMessage = new GridData(
					GridData.HORIZONTAL_ALIGN_CENTER);
			postMessage.setLayoutData(gdPostMessage);
			postMessage.setText(Constants.MVN_NOT_FOUND3);
			postMessage.setForeground(new Color(null, 255, 0, 0));

			setPageComplete(false);

			browse = new Button(container, SWT.CENTER);
			browse.setText(Constants.BROWSE_BUTTON);
			GridData gd3 = new GridData(GridData.CENTER, GridData.CENTER, true,
					true);
			browse.setLayoutData(gd3);
			fileDialog = new FileDialog(getShell());
			browse.addSelectionListener(new SelectionListener() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					widgetDefaultSelected(e);
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					String mvnPath = fileDialog.open();
					if (mvnPath != null) {
						if (testMaven(mvnPath)) {
							preMessage.setText(Constants.MVN_FOUND);
							preMessage
									.setForeground(new Color(null, 0, 255, 0));
							messageLink.setVisible(false);
							postMessage.setVisible(false);
							browse.setVisible(false);
							Activator.getDefault().getPreferenceStore()
									.setValue(Constants.MVN_PATH, mvnPath);
							setPageComplete(true);
						} else {
							preMessage.setText(Constants.MVN_NOT_FOUND);
							preMessage
									.setForeground(new Color(null, 255, 0, 0));
							messageLink.setText(Constants.MVN_NOT_FOUND2);
							messageLink
									.setForeground(new Color(null, 255, 0, 0));
							messageLink.setVisible(true);
							postMessage.setText(Constants.MVN_NOT_FOUND3);
							postMessage
									.setForeground(new Color(null, 255, 0, 0));
							postMessage.setVisible(true);
							browse.setVisible(true);
							setPageComplete(false);
						}
					} else {
						preMessage.setText(Constants.MVN_NOT_FOUND);
						preMessage.setForeground(new Color(null, 255, 0, 0));
						messageLink.setText(Constants.MVN_NOT_FOUND2);
						messageLink.setForeground(new Color(null, 255, 0, 0));
						messageLink.setVisible(true);
						postMessage.setText(Constants.MVN_NOT_FOUND3);
						postMessage.setForeground(new Color(null, 255, 0, 0));
						postMessage.setVisible(true);
						browse.setVisible(true);
						setPageComplete(false);
					}
				}
			});
		} else {
			setPageComplete(true);
		}

		if (JavaCore.getClasspathVariable("M2_REPO") == null
				|| JavaCore.getClasspathVariable("M2_REPO").isEmpty()) {
			repoLabel = new Label(container, SWT.CENTER);
			GridData gd2 = new GridData(GridData.FILL_HORIZONTAL);
			repoLabel.setLayoutData(gd2);
			repoLabel.setForeground(new Color(null, 255, 150, 0));
			repoLabel
					.setText("WARNING : The variable M2_REPO is not correctly set in ClassPath Variables");
		}

		// Label fill = new Label(container, SWT.CENTER);
		// GridData gd3 = new GridData(GridData.FILL_HORIZONTAL);
		// fill.setLayoutData(gd3);
		// fill.setText(".");

		Label version = new Label(container, SWT.NONE);
		version.setText("version "
				+ Activator.getDefault().getBundle().getVersion());
		gd = new GridData(GridData.VERTICAL_ALIGN_END
				| GridData.HORIZONTAL_ALIGN_END | GridData.FILL_VERTICAL);
		version.setLayoutData(gd);

		// Required to avoid an error in the system
		setControl(container);
	}
}
