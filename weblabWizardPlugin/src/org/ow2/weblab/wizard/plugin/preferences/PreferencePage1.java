/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2012 Cassidian, an EADS company
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.wizard.plugin.preferences;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.ow2.weblab.wizard.plugin.Activator;
import org.ow2.weblab.wizard.plugin.utils.Constants;


/**
 * WebLab Settings Preference Page
 * @author Clément Caron - Cassidian
 */
public class PreferencePage1 extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	public PreferencePage1() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription(Constants.PREFERENCE_PAGE1_DESCRIPTION);
	}

	public PreferencePage1(int style) {
		super(style);
	}

	public PreferencePage1(String title, int style) {
		super(title, style);
	}

	public PreferencePage1(String title, ImageDescriptor image, int style) {
		super(title, image, style);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 * @author Clément Caron - Cassidian
	 * Display a tiny logo on the top of the window
	 */
	@Override
	public void init(IWorkbench workbench) {
		super.setImageDescriptor(ImageDescriptor.createFromURL(getClass()
				.getResource(Constants.TINY_WEBLAB_LOGO)));

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 * @author Clément Caron - Cassidian
	 */
	@Override
	protected void createFieldEditors() {
		addField(new FileFieldEditor(Constants.MVN_PATH, Constants.MVN_PATH_DESC, getFieldEditorParent()));
		
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 2;
		
		new Label(getFieldEditorParent(), 0);//Crapy...
		
		Link link = new Link(getFieldEditorParent(), SWT.CENTER);
		link.setLayoutData(gd);
		link.setText("Apache Maven can be found <a href=\"http://maven.apache.org/\">here</a>.");
		link.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				try {
					PlatformUI.getWorkbench().getBrowserSupport()
							.getExternalBrowser()
							.openURL(new URL("http://maven.apache.org/"));
				} catch (PartInitException ex) {
					ex.printStackTrace();
				} catch (MalformedURLException ex) {
					ex.printStackTrace();
				}
			}
		});
		
		addField(new FileFieldEditor(Constants.MVN_SETTINGS, Constants.MVN_SETTINGS_DESC, getFieldEditorParent()));
		addField(new StringFieldEditor(Constants.MVN_MAVEN_OPTS, Constants.MVN_MAVEN_OPTS_DESC, getFieldEditorParent()));
		addField(new StringFieldEditor(Constants.MVN_CREATE_OPTS, Constants.MVN_CREATE_OPTS_DESC, getFieldEditorParent()));
		addField(new StringFieldEditor(Constants.MVN_ECLIPSE_OPTS, Constants.MVN_ECLIPSE_OPTS_DESC, getFieldEditorParent()));
		addField(new StringFieldEditor(Constants.MVN_PACKAGE_OPTS, Constants.MVN_PACKAGE_OPTS_DESC, getFieldEditorParent()));
		addField(new StringFieldEditor(Constants.MVN_INSTALL_OPTS, Constants.MVN_INSTALL_OPTS_DESC, getFieldEditorParent()));
	}
	
//	@Override
//	protected void checkState() {
//		setValid(true);
//	}

}