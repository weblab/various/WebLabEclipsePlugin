/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2012 Cassidian, an EADS company
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.wizard.plugin.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.ow2.weblab.wizard.plugin.Activator;
import org.ow2.weblab.wizard.plugin.utils.CatalogEditor;
import org.ow2.weblab.wizard.plugin.utils.Constants;


/**
 * WebLab Catalog Preference Page
 * @author Clément Caron - Cassidian
 */
public class PreferencePage2 extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	public PreferencePage2() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription(Constants.PREFERENCE_PAGE2_DESCRIPTION);
	}
	
	public PreferencePage2(int style) {
		super(style);
	}

	public PreferencePage2(String title, int style) {
		super(title, style);
	}

	public PreferencePage2(String title, ImageDescriptor image, int style) {
		super(title, image, style);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 * @author Clément Caron - Cassidian
	 * Display a tiny logo on the top of the window
	 */
	@Override
	public void init(IWorkbench workbench) {
		super.setImageDescriptor(ImageDescriptor.createFromURL(getClass()
				.getResource(Constants.TINY_WEBLAB_LOGO)));
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 * @author Clément Caron - Cassidian
	 */
	@Override
	protected void createFieldEditors() {
		addField(new CatalogEditor(Constants.CATALOG_SETTINGS, Constants.CATALOG_SETTINGS_DESC, getFieldEditorParent()));
	}
}
