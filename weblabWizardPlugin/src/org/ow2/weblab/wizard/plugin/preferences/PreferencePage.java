/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2012 Cassidian, an EADS company
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.wizard.plugin.preferences;

import java.io.IOException;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.ow2.weblab.wizard.plugin.Activator;
import org.ow2.weblab.wizard.plugin.utils.Constants;


/**
 * WebLab Main Preference page
 * @author Clément Caron - Cassidian
 */
public class PreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {
	private Image image = null;

	public PreferencePage() {
	}

	public PreferencePage(int style) {
		super(style);
	}

	public PreferencePage(String title, int style) {
		super(title, style);
	}

	public PreferencePage(String title, ImageDescriptor image, int style) {
		super(title, image, style);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 * @author Clément Caron - Cassidian
	 * Display a tiny logo on the top of the window
	 */
	@Override
	public void init(IWorkbench workbench) {
		super.setImageDescriptor(ImageDescriptor.createFromURL(getClass()
				.getResource(Constants.TINY_WEBLAB_LOGO)));
	}

	/* (non-javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 * @author Clément Caron - Cassidian
	 * Display the logo on the FieldEditor Grand-Parent
	 */
	@Override
	protected void createFieldEditors() {
		//get image from resources
		try {
			image = new Image(null, getClass().getResource(Constants.WEBLAB_LOGO).openStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(image != null) {
			
			getFieldEditorParent().getParent().addPaintListener(new PaintListener() {
				
				@Override
				public void paintControl(PaintEvent e) {
					
					//Hide Children
					Control[] control = getFieldEditorParent().getParent().getChildren();
					for (int i = 0; i < control.length; i++) {
						if(control[i] instanceof Label) {
							Label l = (Label) control[i];
							if(!l.getText().startsWith("version")) {
								control[i].setVisible(false);
							}
						}
						else {
							control[i].setVisible(false);
						}
					}
					
					//Display the logo in the center of the shell
					e.gc.drawImage(image, 
							getFieldEditorParent().getParent().getSize().x / 2 - image.getBounds().width / 2,
							getFieldEditorParent().getParent().getSize().y / 2 - image.getBounds().height / 2);
				}
			});
		}
		
		Label version = new Label(getFieldEditorParent().getParent(), SWT.NONE);
		version.setText("version " + Activator.getDefault().getBundle().getVersion());
		GridData gd = new GridData(GridData.FILL_VERTICAL | GridData.VERTICAL_ALIGN_END | GridData.HORIZONTAL_ALIGN_END);
		version.setLayoutData(gd);
	}
}
