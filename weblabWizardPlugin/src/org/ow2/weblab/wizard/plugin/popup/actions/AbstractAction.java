/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2012 Cassidian, an EADS company
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.wizard.plugin.popup.actions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.junit.JUnitCore;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.ow2.weblab.wizard.plugin.Activator;
import org.ow2.weblab.wizard.plugin.utils.Constants;
import org.ow2.weblab.wizard.plugin.utils.PopupDialog;
import org.ow2.weblab.wizard.plugin.utils.SurefireFilter;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public abstract class AbstractAction implements IObjectActionDelegate {

	private Shell shell;
	private ISelection selection;
	private ProcessBuilder builder;
	private Status status;
	private ILog log = Activator.getDefault().getLog();
	Process proc;
	
	private List<String> command;
	private String specialOptions;
	
	/**
	 * Constructor for Action1.
	 */
	protected AbstractAction() {
		super();
	}
	
	protected void setCommands(List<String> command, String options) {
		this.command = new ArrayList<String>();
		this.command.addAll(command);
		specialOptions = options;
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	@SuppressWarnings("restriction")
	public void run(IAction action) {
		final String mvnPath = Activator.getDefault().getPreferenceStore().getString(Constants.MVN_PATH);
		String settings = Activator.getDefault().getPreferenceStore().getString(Constants.MVN_SETTINGS);
		String mavenOptions = Activator.getDefault().getPreferenceStore().getString(Constants.MVN_MAVEN_OPTS);
		File file = null;
		boolean isJavaWorking = isJavaWorking();
		String javaHome = getJavaHome();
		
		if(selection instanceof org.eclipse.jface.viewers.TreeSelection) {
			org.eclipse.jface.viewers.TreeSelection obj = (org.eclipse.jface.viewers.TreeSelection) selection;
			org.eclipse.core.resources.IProject project = null;
			
			if(obj.getFirstElement() instanceof org.eclipse.core.resources.IProject) {
				project = (org.eclipse.core.resources.IProject) obj.getFirstElement();
			}
			else if(obj.getFirstElement() instanceof org.eclipse.jdt.internal.core.JavaProject) {
				project = ((org.eclipse.jdt.internal.core.JavaProject) obj.getFirstElement()).getProject();
			}
			else {
				MessageDialog.openError(shell, "Error", "Project type not recognised");
				return;
			}
			
			if(project != null) {
				if(project.isOpen()){
					if(project.getProject().getRawLocationURI() != null) {
						file = new File(project.getProject().getRawLocationURI().getPath());
					}
					else {
						file = Platform.getLocation().append(project.getFullPath()).toFile();
					}
				}
				else {
					MessageDialog.openError(shell, "Error", "Project is closed");
					return;
				}
			}
		}
		
		if(file != null && file.exists()) {
			final File finalFile = file;
			List<String> currentCommand = new ArrayList<String>();
			if(mvnPath != null && !mvnPath.equals(""))
				currentCommand.add(mvnPath);
			else
				currentCommand.add(Constants.MVN);
			
			currentCommand.addAll(command);
			
			if(settings != null && !settings.equals("")) {
				currentCommand.add("--settings");
				currentCommand.add(settings);
			}
			else {
				try {
					File set = createTempSettings();// dirty, but only way to have absolute path to the settings file
					currentCommand.add("--settings");
					currentCommand.add(set.getAbsolutePath());
				} catch (IOException e) {
					PopupDialog.openWarning(shell, "Error", "Problem while generating settings.xml", e.getMessage());
				}
			}
			
			if(specialOptions != null && !specialOptions.equals("")) {
				currentCommand.add(specialOptions);
			}
			
			builder = new ProcessBuilder(currentCommand);
			Map<String, String> env = builder.environment();
			env.put("JAVA_HOME", javaHome);
			
			if(!isJavaWorking) {
				String systemPath = env.get("PATH");
				systemPath = javaHome.concat(File.separator).concat("bin").concat(File.pathSeparator).concat(systemPath);
				env.remove("PATH");
				env.put("PATH", systemPath);
			}
			
			if(mavenOptions != null && !mavenOptions.equals("")) {
				env.put("MAVEN_OPTS", mavenOptions);
			}
			
			builder.directory(finalFile);
			status = new Status(IStatus.INFO, Activator.PLUGIN_ID, "Command:\n" + builder.command() + " in " + builder.directory());
			log.log(status);
			
			Job job = new Job("WebLab Command Running") {
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					
					monitor.beginTask("WebLab Command", 100);
					monitor.worked(1);
					try {
						if(!testMaven(mvnPath)) {
							Display.getDefault().asyncExec(new Runnable() {
								public void run() {
									MessageDialog.openError(shell, "Error", "Maven was not found. Please set it properly in the WebLab preferencies.");
								}
							});
							return Status.CANCEL_STATUS;
						}
						
						proc = builder.start();
						
						InputStream stream = proc.getInputStream();
						BufferedReader br = new BufferedReader(new InputStreamReader(stream));
						String line = "";
						
						String message = "";
						int percent = 98;
						while((line = br.readLine()) != null) {
							message = message.concat(line + "\n");
							if(percent > 0) {
								monitor.worked(1);
								percent--;
							}
						}
						status = new Status(IStatus.INFO, Activator.PLUGIN_ID, message);
						log.log(status);
						
						proc.waitFor();
						
						if(proc.exitValue() != 0) {
							File surefire = new File(finalFile.getAbsolutePath() 
									+ File.separator + "target" 
									+ File.separator + "surefire-reports");
							
							final String finalMessage = message;
							if(surefire.exists()) {
								long lastMod = Long.MIN_VALUE;
								File choise = null;
								SurefireFilter filter = new SurefireFilter();
								for (File file : surefire.listFiles(filter)) {
									if (file.lastModified() > lastMod) {
										choise = file;
										lastMod = file.lastModified();
									}
								}
								
								if(choise != null) {
									final File surefireFile = choise;
									final File javaTestPath = new File(finalFile.getAbsolutePath()
											+ File.separator + "src" 
											+ File.separator + "test" 
											+ File.separator + "java");
											
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											PopupDialog.openWarning(shell, "Warning", "Something went wrong during the JUnit tests", finalMessage);
											try {
												JUnitCore.importTestRunSession(surefireFile);
												try {
													PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView("org.eclipse.jdt.junit.ResultView");
												}
												catch (Exception e) {
													e.printStackTrace();
												}
												
												try {
													Document dom;
													DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
													DocumentBuilder db = dbf.newDocumentBuilder();
													dom = db.parse(surefireFile);
													
													String testPath = "";
													if(dom.getElementsByTagName("testsuite").getLength()>0) {
														Node n = dom.getElementsByTagName("testsuite").item(0);
														Node name = n.getAttributes().getNamedItem("name");
														testPath = name.getNodeValue();
													}
													
													if(testPath != null && !testPath.equals("")) {
														testPath = File.separator + testPath.replace(".", File.separator) + ".java";
														File javaFile = new File(javaTestPath.getAbsolutePath() + testPath);
														IFileStore fileStore = EFS.getLocalFileSystem().getStore(javaFile.toURI());
														IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
														org.eclipse.ui.ide.IDE.openInternalEditorOnFileStore(page, fileStore);
													}
												}
												catch (SAXException e) {
													e.printStackTrace();
												} catch (IOException e) {
													e.printStackTrace();
												} catch (ParserConfigurationException e) {
													e.printStackTrace();
												}
											} catch (CoreException e) {
												e.printStackTrace();
											}
										}
									});
								}
								else {
									Display.getDefault().asyncExec(new Runnable() {
										public void run() {
											PopupDialog.openWarning(shell, "Warning", "Something went wrong during the JUnit tests", finalMessage);
										}
									});
								}
							}
							else {
								Display.getDefault().asyncExec(new Runnable() {
									public void run() {
										PopupDialog.openWarning(shell, "Warning", "Something went wrong during the " + builder.command() + " command", finalMessage);
									}
								});
							}
						}
					} catch (Exception e) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								MessageDialog.openError(shell, "Error", Constants.MVN_ERROR);
							}
						});
						e.printStackTrace();
					}
					monitor.done();
					return Status.OK_STATUS;
				}
			};

			job.schedule();
			
			
		}
		else {
			MessageDialog.openError(shell, "Error", "Folder " + file.getAbsolutePath() + "does not exist");
		}
	}
	
	private boolean isJavaWorking() {
		boolean result = false;
		
		try {
			List<String> command = new ArrayList<String>();
			
			command.add("java");
			command.add("-version");
			
			ProcessBuilder javaTest = new ProcessBuilder(command);
		
			Process proc = javaTest.start();
			proc.waitFor();
			if(proc.exitValue() == 0) {
				result = true;
			}
		} 
		catch (Exception e) { }
		
		return result;
	}

	private String getJavaHome() {
		if(System.getenv("JAVA_HOME") == null) {
			return(JavaRuntime.getDefaultVMInstall().getInstallLocation().getAbsolutePath());
		}
		else {
			return(System.getenv("JAVA_HOME"));
		}
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	@SuppressWarnings("restriction")
	public void selectionChanged(IAction action, ISelection selection) {
		
		if(selection instanceof org.eclipse.jface.viewers.TreeSelection) {
			org.eclipse.jface.viewers.TreeSelection obj = (org.eclipse.jface.viewers.TreeSelection) selection;
			if(obj.getFirstElement() instanceof org.eclipse.core.resources.IProject) {
				org.eclipse.core.resources.IProject project = (org.eclipse.core.resources.IProject) obj.getFirstElement();
				if(project.isOpen()){
					action.setEnabled(true);
				}
				else {
					action.setEnabled(false);
				}
			}
			else if(obj.getFirstElement() instanceof org.eclipse.jdt.internal.core.JavaProject){
				action.setEnabled(true);
			}
		}
		
		this.selection = selection;
	}
	
	private File createTempSettings() throws IOException {
		File result = File.createTempFile(Constants.PREFIX_SETTINGS, Constants.SUFFIX_SETTINGS);
		InputStream in = getClass().getResource(Constants.DEFAULT_SETTINGS).openStream();
		StringWriter sWriter = new StringWriter();
	    FileWriter out = new FileWriter(result);
	    int c;

	    while ((c = in.read()) != -1)
	    	sWriter.write(c);

	    in.close();
	    sWriter.close();
	    
	    String str = sWriter.toString();
	    
	    if (JavaCore.getClasspathVariable("M2_REPO") != null
				&& !JavaCore.getClasspathVariable("M2_REPO").isEmpty()) {
	    	String settingsItem = "<!-- localRepository -->";
		    String localRepo = "<localRepository>" + JavaCore.getClasspathVariable("M2_REPO") + "</localRepository>";
		    str = str.replace(settingsItem, localRepo);
	    }
	    
	    out.write(str);
	    out.close();
	    
		return result;
	}
	
	private boolean testMaven(String path) {
		try {
			Process proc = Runtime.getRuntime().exec(path + Constants.MVN_TEST);
			proc.waitFor();
			if (proc.exitValue() == 0) {
				return true;
			}
		} catch (IOException e) {
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return false;
	}

}
